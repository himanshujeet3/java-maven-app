def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t jeethim/demo-app:jma-2.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push jeethim/demo-app:jma-2.0'
    }
} 

def deployApp() {
    echo 'deploying the application...'
    def docker_cmd= 'docker run -d -p 8080:8080 jeethim/demo-app:jma-2.0'
    sshagent(['ec2-server']){
       sh "ssh -o StrictHostKeyChecking=no root@3.109.200.177 $(docker_cmd)"

    }
    
} 

return this
